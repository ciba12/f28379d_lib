//#############################################################################
//
// FILE:   spi_ex1_loopback.c
//
// TITLE:  SPI Digital Loopback
//
//! \addtogroup driver_example_list
//! <h1>SPI Digital Loopback</h1>
//!
//! This program uses the internal loopback test mode of the SPI module. This
//! is a very basic loopback that does not use the FIFOs or interrupts. A
//! stream of data is sent and then compared to the received stream.
//!
//! The sent data looks like this: \n
//!  0000 0001 0002 0003 0004 0005 0006 0007 .... FFFE FFFF 0000
//!
//! This pattern is repeated forever.
//!
//! \b External \b Connections \n
//!  - None
//!
//! \b Watch \b Variables \n
//!  - \b sData - Data to send
//!  - \b rData - Received data
//!
//
//#############################################################################
// $TI Release: F2837xD Support Library v3.05.00.00 $
// $Release Date: Tue Jun 26 03:15:23 CDT 2018 $
// $Copyright:
// Copyright (C) 2013-2018 Texas Instruments Incorporated - http://www.ti.com/
//
// Redistribution and use in source and binary forms, with or without 
// modification, are permitted provided that the following conditions 
// are met:
// 
//   Redistributions of source code must retain the above copyright 
//   notice, this list of conditions and the following disclaimer.
// 
//   Redistributions in binary form must reproduce the above copyright
//   notice, this list of conditions and the following disclaimer in the 
//   documentation and/or other materials provided with the   
//   distribution.
// 
//   Neither the name of Texas Instruments Incorporated nor the names of
//   its contributors may be used to endorse or promote products derived
//   from this software without specific prior written permission.
// 
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
// "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT 
// LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
// A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT 
// OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, 
// SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT 
// LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
// DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
// THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT 
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE 
// OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
// $
//#############################################################################

//
// Included Files
//
#include <eMD-6.0/driver/eMPL/driver_api.h>
#include <eMD-6.0/driver/eMPL/inv_mpu.h>
#include <eMD-6.0/driver/eMPL/inv_mpu_dmp_motion_driver.h>
#include "driverlib.h"
#include "device.h"
#include "spi.h"
#include <math.h>
#include "pin_configurations.h"
#include "stdio.h"

/* These next two functions converts the orientation matrix (see
 * gyro_orientation) to a scalar representation for use by the DMP.
 * NOTE: These functions are borrowed from Invensense's MPL.
 */


#define DEFAULT_MPU_HZ  (100)
#define ACCEL_SCALE_RANGE 16 // 2,4,8,16 [g]
#define GYRO_SCALE_RANGE 250 // 250,500,1000,2000 [deg/s]


static inline unsigned short inv_row_2_scale(const signed char *row)
{
	unsigned short b;

	if (row[0] > 0)
		b = 0;
	else if (row[0] < 0)
		b = 4;
	else if (row[1] > 0)
		b = 1;
	else if (row[1] < 0)
		b = 5;
	else if (row[2] > 0)
		b = 2;
	else if (row[2] < 0)
		b = 6;
	else
		b = 7;      // error
	return b;
}
static signed char gyro_orientation[9] = { -1, 0, 0, 0, -1, 0, 0, 0, 1 };
static inline unsigned short inv_orientation_matrix_to_scalar(const signed char *mtx)
{
	unsigned short scalar;

	/*
	 XYZ  010_001_000 Identity Matrix
	 XZY  001_010_000
	 YXZ  010_000_001
	 YZX  000_010_001
	 ZXY  001_000_010
	 ZYX  000_001_010
	 */

	scalar = inv_row_2_scale(mtx);
	scalar |= inv_row_2_scale(mtx + 3) << 3;
	scalar |= inv_row_2_scale(mtx + 6) << 6;

	return scalar;
}

// Main
//
void main(void)
{
//    uint16_t sData = 0;                  // Send data
//    uint16_t rData = 0;                  // Receive data

//
// Initialize device clock and peripherals
//
	Device_init();

	//
	// Disable pin locks and enable internal pullups.
	//
	Device_initGPIO();

	//
	// Initialize PIE and clear PIE registers. Disables CPU interrupts.
	//
	Interrupt_initModule();

	//
	// Initialize the PIE vector table with pointers to the shell Interrupt
	// Service Routines (ISR).
	//
	Interrupt_initVectorTable();

	//
	// Set up SPI, initializing it for FIFO mode
	//

	configGPIOs();
	initSPI();

	//
	// Enable Global Interrupt (INTM) and realtime interrupt (DBGM)
	//
	EINT;
	ERTM;


// multiply data from MPU by this to get it in [deg/s] and [g]
	const float accel_scale = (2 * ACCEL_SCALE_RANGE) / 65536.0;
	const float gyro_scale = (2 * GYRO_SCALE_RANGE) / 65536.0;

	mpu_init(NULL);

	/* Get/set hardware configuration. Start gyro. */
	/* Wake up all sensors. */
	mpu_set_sensors(INV_XYZ_GYRO | INV_XYZ_ACCEL);
	/* Push both gyro and accel data into the FIFO. */
	mpu_configure_fifo(INV_XYZ_GYRO | INV_XYZ_ACCEL);
	mpu_set_sample_rate(DEFAULT_MPU_HZ);
	mpu_set_gyro_fsr(GYRO_SCALE_RANGE);

	// DMP

	dmp_load_motion_driver_firmware();
	dmp_set_orientation(inv_orientation_matrix_to_scalar(gyro_orientation));
	dmp_register_tap_cb(NULL);
	dmp_register_android_orient_cb(NULL);

	dmp_enable_feature(DMP_FEATURE_6X_LP_QUAT | DMP_FEATURE_TAP |
	DMP_FEATURE_ANDROID_ORIENT | DMP_FEATURE_SEND_RAW_ACCEL | DMP_FEATURE_SEND_CAL_GYRO |
	DMP_FEATURE_GYRO_CAL);
	dmp_set_fifo_rate(DEFAULT_MPU_HZ);
	mpu_set_dmp_state(1);
	int cal = dmp_enable_gyro_cal(1);
	int ret_int = dmp_set_interrupt_mode(DMP_INT_CONTINUOUS);

	int16_t gyro[3], accel[3], sensors;
	long quat[4];
	uint16_t more;
	unsigned long sensor_timestamp;
	dmp_read_fifo(gyro, accel, quat, &sensor_timestamp, &sensors, &more);
	dmp_read_fifo(gyro, accel, quat, &sensor_timestamp, &sensors, &more);
	dmp_read_fifo(gyro, accel, quat, &sensor_timestamp, &sensors, &more);
	dmp_read_fifo(gyro, accel, quat, &sensor_timestamp, &sensors, &more);

	uint16_t range = 0;

	mpu_set_accel_fsr(2);
	spi_read(0, 28, 1, &range);
	mpu_set_accel_fsr(4);
	spi_read(0, 28, 1, &range);
	mpu_set_accel_fsr(8);
	spi_read(0, 28, 1, &range);

	int16_t data_acc_2[3];
	mpu_get_accel_reg(data_acc_2, NULL);
	mpu_get_accel_reg(data_acc_2, NULL);
	mpu_get_accel_reg(data_acc_2, NULL);
	mpu_get_accel_reg(data_acc_2, NULL);

	mpu_set_accel_fsr(16);
	int16_t data_acc_16[3];
	mpu_get_accel_reg(data_acc_16, NULL);
	mpu_get_accel_reg(data_acc_16, NULL);
	mpu_get_accel_reg(data_acc_16, NULL);
	mpu_get_accel_reg(data_acc_16, NULL);

}

