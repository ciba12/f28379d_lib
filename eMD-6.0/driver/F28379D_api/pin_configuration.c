/*
 * gpio_configurations.c
 *
 *  Created on: Sep 26, 2018
 *      Author: ciba
 */

// if you want to add more SPI chips, search for lines with ADD_SPI_HERE

#include "pin_configurations.h"
#include "spi.h"
#include "device.h"
#include "driverlib.h"

// define polarity,frequency and message length
#define SPIA_POLARITY SPI_PROT_POL1PHA0
#define SPIA_FREQUENCY 1000000 // [Hz] MPU6000 can do 1Mhz write and 20Mhz read
#define SPIA_NUMBER_OF_BITS 8
//ADD_SPI_HERE


// returns GPIO number of chip-select pin for a given slave_address
// slave_address is my custom name for MPU0, MPU1, ....
int32_t chip_select_pin(uint16_t slave_addr){
	switch(slave_addr){
		case 0:
			return 61; // MPU0 - GPIO61, (#19)
		default: //ADD_SPI_HERE
			return -1;
	}
}

// configures SPI pins
void configGPIOs(void)
{


	// SPI A
	//------------------

    // GPIO59 (14) - SPISOMI
    GPIO_setMasterCore(59, GPIO_CORE_CPU1);
    GPIO_setPinConfig(GPIO_59_SPISOMIA);
    GPIO_setPadConfig(59, GPIO_PIN_TYPE_PULLUP);
    GPIO_setQualificationMode(59, GPIO_QUAL_ASYNC);

    // GPIO58 (15) - SPISIMO
    GPIO_setMasterCore(58, GPIO_CORE_CPU1);
    GPIO_setPinConfig(GPIO_58_SPISIMOA);
    GPIO_setPadConfig(58, GPIO_PIN_TYPE_PULLUP);
    GPIO_setQualificationMode(58, GPIO_QUAL_ASYNC);

    // GPIO60 (7) - SPICLK
    GPIO_setMasterCore(60, GPIO_CORE_CPU1);
    GPIO_setPinConfig(GPIO_60_SPICLKA);
    GPIO_setPadConfig(60, GPIO_PIN_TYPE_PULLUP);
    GPIO_setQualificationMode(60, GPIO_QUAL_ASYNC);

    //    MPU specific chip selects
    //-----------------

    // GPIO61 (19) - chip-select - pin will be driven manually by functions select() and deselect()
    GPIO_setMasterCore(61, GPIO_CORE_CPU1);
    GPIO_setPinConfig(GPIO_61_GPIO61);
    GPIO_setPadConfig(61, GPIO_PIN_TYPE_STD); //push-pull output
    GPIO_setDirectionMode(61,GPIO_DIR_MODE_OUT);


    //ADD_SPI_HERE

}



//
// Function to configure SPI
//
void initSPI()
{
	// SPI A
    //
    // Must put SPI into reset before configuring it
    //
    SPI_disableModule(SPIA_BASE);
    SPI_disableHighSpeedMode(SPIA_BASE);

    // MPU6000 has SPI_PROT_POL1PHA0 - falling edge without delay. See the docs (page 19)
    SPI_setConfig(SPIA_BASE, DEVICE_LSPCLK_FREQ, SPIA_POLARITY,
                      SPI_MODE_MASTER, SPIA_FREQUENCY, SPIA_NUMBER_OF_BITS);

    SPI_disableLoopback(SPIA_BASE);
    SPI_disableHighSpeedMode(SPIA_BASE);
    SPI_setEmulationMode(SPIA_BASE, SPI_EMULATION_STOP_AFTER_TRANSMIT);

    //
    // Configuration complete. Enable the module.
    //
    SPI_enableModule(SPIA_BASE);




    //ADD_SPI_HERE

}
