/*
 * gpio_configurations.h
 *
 *  Created on: Sep 26, 2018
 *      Author: ciba
 */

#ifndef PIN_CONFIGURATIONS_H_
#define PIN_CONFIGURATIONS_H_

#include <stdint.h>

int32_t chip_select_pin(uint16_t slave_addr);
void configGPIOs(void);
void initSPI();


#endif /* PIN_CONFIGURATIONS_H_ */
