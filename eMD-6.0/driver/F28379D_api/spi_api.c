/*
 * spi_api.c
 *
 *  Created on: Sep 26, 2018
 *      Author: ciba
 */
#include "spi.h"
#include "device.h"
#include "driverlib.h"
#include "pin_configurations.h"
#include "spi_api.h"

void select(uint16_t slave_addr){
	GPIO_writePin(chip_select_pin(slave_addr),0U);
}
void deselect(uint16_t slave_addr){
	GPIO_writePin(chip_select_pin(slave_addr),1);
}

// sends one packet via SPI, assumes that the device is already selected
uint16_t spi_send_packet_api(uint64_t spi_base, uint16_t data, uint16_t SPIx_number_of_bits){
    // shift the data to the left
	data = data << (16-SPIx_number_of_bits);
    SPI_writeDataNonBlocking(spi_base, data);
//    SPI_writeDataBlockingNonFIFO(SPIA_BASE,data);
    uint16_t rData = SPI_readDataBlockingNonFIFO(spi_base);
    return rData;
}

/*
 * writes data via SPI
 * Input:
 * spi_base - eg SPIA_BASE,SPIB_BASE....
 * slave_addr - slave address: 0,1,2...
 * data_send - pointer to array of data to send via SPI
 * number_of_packets - length of the array
 * data_received - pointer, received data will be stored here
 * SPIx_number_of_bits - length of one packet
 */
void spi_write_api(uint64_t spi_base, uint16_t slave_addr ,uint16_t* data_send, uint16_t number_of_packets,
		uint16_t* data_received, uint16_t SPIx_number_of_bits){
	select(slave_addr);
	uint16_t k = 0;

	for(k = 0;k < number_of_packets;k++){
		data_received[k] = spi_send_packet_api(spi_base,data_send[k], SPIx_number_of_bits);
	}
	deselect(slave_addr);
}

/*
 * writes data via SPI
 * Input:
 * spi_base - eg SPIA_BASE,SPIB_BASE....
 * slave_addr - slave address: 0,1,2...
 * data_send - pointer to array of data to send via SPI
 * number_of_packets - length of the array
 * data_received - pointer, received data will be stored here
 * SPIx_number_of_bits - length of one packet
 */
void spi_write2addr_api(uint64_t spi_base, uint16_t slave_addr ,uint16_t registry_addr ,uint16_t* data_send, uint16_t number_of_packets,
		uint16_t* data_received, uint16_t SPIx_number_of_bits){
	select(slave_addr);
	uint16_t k = 0;

	spi_send_packet_api(spi_base,registry_addr, SPIx_number_of_bits); // set reg. address
	for(k = 0;k < number_of_packets;k++){
		data_received[k] = spi_send_packet_api(spi_base,data_send[k], SPIx_number_of_bits);
	}
	deselect(slave_addr);
}





