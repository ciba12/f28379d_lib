/*
 * spi_api.h
 *
 *  Created on: Sep 26, 2018
 *      Author: ciba
 */

#ifndef SPI_API_H_
#define SPI_API_H_

void select(uint16_t slave_addr);
void deselect(uint16_t slave_addr);
uint16_t spi_send_packet_api(uint64_t spi_base, uint16_t data, uint16_t SPIx_number_of_bits);
void spi_write_api(uint64_t spi_base, uint16_t slave_addr ,uint16_t* data_send, uint16_t number_of_packets,
		uint16_t* data_received, uint16_t SPIx_number_of_bits);
void spi_write2addr_api(uint64_t spi_base, uint16_t slave_addr ,uint16_t registry_addr ,uint16_t* data_send, uint16_t number_of_packets,
		uint16_t* data_received, uint16_t SPIx_number_of_bits);
#endif /* SPI_API_H_ */
