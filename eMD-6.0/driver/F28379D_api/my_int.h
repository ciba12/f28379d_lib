/*
 * my_int.h
 *
 *  Created on: Sep 29, 2018
 *      Author: ciba
 */

#ifndef EMD_6_0_DRIVER_F28379D_API_MY_INT_H_
#define EMD_6_0_DRIVER_F28379D_API_MY_INT_H_

typedef   signed char   int8_t;
typedef unsigned char  uint8_t;


#endif /* EMD_6_0_DRIVER_F28379D_API_MY_INT_H_ */
