/*
 * driver_api.c
 *
 *  Created on: Sep 26, 2018
 *      Author: ciba
 */

/* The following functions must be defined for this platform:
 * i2c_write(uint16_t slave_addr, uint16_t reg_addr,
 *      uint16_t length, uint16_t const *data)
 * spi_read(uint16_t slave_addr, uint16_t reg_addr,
 *      uint16_t length, uint16_t *data)
 * delay_ms(unsigned long num_ms)
 * get_ms(unsigned long *count)
 * reg_int_cb(void (*cb)(void), uint16_t port, uint16_t pin)
 * labs(long x)
 * fabsf(float x)
 * min(int a, int b)
 */

#include <eMD-6.0/driver/eMPL/inv_mpu.h>
#include "pin_configurations.h"
#include "spi.h"
#include "device.h"
#include "driverlib.h"
#include "spi_api.h"
#include "stdio.h"
#include "math.h"

#define MPU6000_SPI_MESSAGE_LEN 8
#define READ_FLAG 0x80

int spi_write(uint16_t slave_addr, uint16_t reg_addr, uint16_t length, uint16_t const *data)
{
	spi_write2addr_api(SPIA_BASE, slave_addr,reg_addr, (uint16_t *) data, (uint16_t) length, NULL, MPU6000_SPI_MESSAGE_LEN);
	return 0;
}

int spi_read(uint16_t slave_addr, uint16_t reg_addr, uint16_t length, uint16_t *data)
{
	// we are sending unknown data from *data as dummy bytes, should be ok
	reg_addr = reg_addr | READ_FLAG;
	spi_write2addr_api(SPIA_BASE, slave_addr, reg_addr, (uint16_t *) data, (uint16_t) length, (uint16_t *) data, MPU6000_SPI_MESSAGE_LEN);
	return 0;
}
void delay_ms(unsigned long num_ms)
{
	DEVICE_DELAY_US(num_ms * 1000);
}
uint32_t get_ms(unsigned long *count)
{
	return 0;
}

int reg_int_cb(struct int_param_s* dummy)
{
	return 0;
}

void __no_operation(){
	asm(" nop");
}





