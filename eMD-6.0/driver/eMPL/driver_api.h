/*
 * driver_api.h
 *
 *  Created on: Sep 26, 2018
 *      Author: ciba
 */

#ifndef DRIVER_DRIVER_API_H_
#define DRIVER_DRIVER_API_H_
#include <stdint.h>
#include <math.h>
#include "inv_mpu.h"

#define labs(x) (((x)<0L)?(-x):(x))
#define fabs(x) fabsf(x)
#define min(a,b) ((a<b)?a:b)

int spi_write(uint16_t slave_addr, uint16_t reg_addr, uint16_t length, uint16_t  *data);
int spi_read(uint16_t slave_addr, uint16_t reg_addr, uint16_t length, uint16_t *data);
void delay_ms(unsigned long num_ms);
uint32_t get_ms(unsigned long *count);
int reg_int_cb(struct int_param_s* dummy);
void __no_operation();

#endif /* DRIVER_DRIVER_API_H_ */
