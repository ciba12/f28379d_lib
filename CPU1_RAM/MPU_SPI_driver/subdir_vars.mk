################################################################################
# Automatically-generated file. Do not edit!
################################################################################

SHELL = cmd.exe

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../MPU_SPI_driver/driver_api.c \
../MPU_SPI_driver/inv_mpu.c \
../MPU_SPI_driver/inv_mpu_dmp_motion_driver.c 

C_DEPS += \
./MPU_SPI_driver/driver_api.d \
./MPU_SPI_driver/inv_mpu.d \
./MPU_SPI_driver/inv_mpu_dmp_motion_driver.d 

OBJS += \
./MPU_SPI_driver/driver_api.obj \
./MPU_SPI_driver/inv_mpu.obj \
./MPU_SPI_driver/inv_mpu_dmp_motion_driver.obj 

OBJS__QUOTED += \
"MPU_SPI_driver\driver_api.obj" \
"MPU_SPI_driver\inv_mpu.obj" \
"MPU_SPI_driver\inv_mpu_dmp_motion_driver.obj" 

C_DEPS__QUOTED += \
"MPU_SPI_driver\driver_api.d" \
"MPU_SPI_driver\inv_mpu.d" \
"MPU_SPI_driver\inv_mpu_dmp_motion_driver.d" 

C_SRCS__QUOTED += \
"../MPU_SPI_driver/driver_api.c" \
"../MPU_SPI_driver/inv_mpu.c" \
"../MPU_SPI_driver/inv_mpu_dmp_motion_driver.c" 


