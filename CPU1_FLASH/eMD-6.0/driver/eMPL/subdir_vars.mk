################################################################################
# Automatically-generated file. Do not edit!
################################################################################

SHELL = cmd.exe

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../eMD-6.0/driver/eMPL/driver_api.c \
../eMD-6.0/driver/eMPL/inv_mpu.c \
../eMD-6.0/driver/eMPL/inv_mpu_dmp_motion_driver.c 

C_DEPS += \
./eMD-6.0/driver/eMPL/driver_api.d \
./eMD-6.0/driver/eMPL/inv_mpu.d \
./eMD-6.0/driver/eMPL/inv_mpu_dmp_motion_driver.d 

OBJS += \
./eMD-6.0/driver/eMPL/driver_api.obj \
./eMD-6.0/driver/eMPL/inv_mpu.obj \
./eMD-6.0/driver/eMPL/inv_mpu_dmp_motion_driver.obj 

OBJS__QUOTED += \
"eMD-6.0\driver\eMPL\driver_api.obj" \
"eMD-6.0\driver\eMPL\inv_mpu.obj" \
"eMD-6.0\driver\eMPL\inv_mpu_dmp_motion_driver.obj" 

C_DEPS__QUOTED += \
"eMD-6.0\driver\eMPL\driver_api.d" \
"eMD-6.0\driver\eMPL\inv_mpu.d" \
"eMD-6.0\driver\eMPL\inv_mpu_dmp_motion_driver.d" 

C_SRCS__QUOTED += \
"../eMD-6.0/driver/eMPL/driver_api.c" \
"../eMD-6.0/driver/eMPL/inv_mpu.c" \
"../eMD-6.0/driver/eMPL/inv_mpu_dmp_motion_driver.c" 


