################################################################################
# Automatically-generated file. Do not edit!
################################################################################

SHELL = cmd.exe

# Each subdirectory must supply rules for building sources it contributes
eMD-6.0/driver/eMPL/driver_api.obj: ../eMD-6.0/driver/eMPL/driver_api.c $(GEN_OPTS) | $(GEN_HDRS)
	@echo 'Building file: "$<"'
	@echo 'Invoking: C2000 Compiler'
	"C:/ti/ccsv8/tools/compiler/ti-cgt-c2000_18.1.1.LTS/bin/cl2000" -v28 -ml -mt --cla_support=cla1 --float_support=fpu32 --tmu_support=tmu0 --vcu_support=vcu2 -Ooff --include_path="E:/c2000_workspace/MPU6000_SPI_lib" --include_path="E:/c2000_workspace/MPU6000_SPI_lib/eMD-6.0/driver/include" --include_path="E:/c2000_workspace/MPU6000_SPI_lib/eMD-6.0/mllite" --include_path="E:/c2000_workspace/MPU6000_SPI_lib/eMD-6.0/driver/F28379D_api" --include_path="E:/c2000_workspace/MPU6000_SPI_lib/device" --include_path="C:/ti/c2000/C2000Ware_1_00_05_00/driverlib/f2837xd/driverlib" --include_path="C:/ti/ccsv8/tools/compiler/ti-cgt-c2000_18.1.1.LTS/include" --define=EMPL_TARGET_F28379D --define=MPU6050 --define=DEBUG --define=_FLASH --define=CPU1 --diag_suppress=10063 --diag_warning=225 --diag_wrap=off --display_error_number --preproc_with_compile --preproc_dependency="eMD-6.0/driver/eMPL/driver_api.d_raw" --obj_directory="eMD-6.0/driver/eMPL" $(GEN_OPTS__FLAG) "$<"
	@echo 'Finished building: "$<"'
	@echo ' '

eMD-6.0/driver/eMPL/inv_mpu.obj: ../eMD-6.0/driver/eMPL/inv_mpu.c $(GEN_OPTS) | $(GEN_HDRS)
	@echo 'Building file: "$<"'
	@echo 'Invoking: C2000 Compiler'
	"C:/ti/ccsv8/tools/compiler/ti-cgt-c2000_18.1.1.LTS/bin/cl2000" -v28 -ml -mt --cla_support=cla1 --float_support=fpu32 --tmu_support=tmu0 --vcu_support=vcu2 -Ooff --include_path="E:/c2000_workspace/MPU6000_SPI_lib" --include_path="E:/c2000_workspace/MPU6000_SPI_lib/eMD-6.0/driver/include" --include_path="E:/c2000_workspace/MPU6000_SPI_lib/eMD-6.0/mllite" --include_path="E:/c2000_workspace/MPU6000_SPI_lib/eMD-6.0/driver/F28379D_api" --include_path="E:/c2000_workspace/MPU6000_SPI_lib/device" --include_path="C:/ti/c2000/C2000Ware_1_00_05_00/driverlib/f2837xd/driverlib" --include_path="C:/ti/ccsv8/tools/compiler/ti-cgt-c2000_18.1.1.LTS/include" --define=EMPL_TARGET_F28379D --define=MPU6050 --define=DEBUG --define=_FLASH --define=CPU1 --diag_suppress=10063 --diag_warning=225 --diag_wrap=off --display_error_number --preproc_with_compile --preproc_dependency="eMD-6.0/driver/eMPL/inv_mpu.d_raw" --obj_directory="eMD-6.0/driver/eMPL" $(GEN_OPTS__FLAG) "$<"
	@echo 'Finished building: "$<"'
	@echo ' '

eMD-6.0/driver/eMPL/inv_mpu_dmp_motion_driver.obj: ../eMD-6.0/driver/eMPL/inv_mpu_dmp_motion_driver.c $(GEN_OPTS) | $(GEN_HDRS)
	@echo 'Building file: "$<"'
	@echo 'Invoking: C2000 Compiler'
	"C:/ti/ccsv8/tools/compiler/ti-cgt-c2000_18.1.1.LTS/bin/cl2000" -v28 -ml -mt --cla_support=cla1 --float_support=fpu32 --tmu_support=tmu0 --vcu_support=vcu2 -Ooff --include_path="E:/c2000_workspace/MPU6000_SPI_lib" --include_path="E:/c2000_workspace/MPU6000_SPI_lib/eMD-6.0/driver/include" --include_path="E:/c2000_workspace/MPU6000_SPI_lib/eMD-6.0/mllite" --include_path="E:/c2000_workspace/MPU6000_SPI_lib/eMD-6.0/driver/F28379D_api" --include_path="E:/c2000_workspace/MPU6000_SPI_lib/device" --include_path="C:/ti/c2000/C2000Ware_1_00_05_00/driverlib/f2837xd/driverlib" --include_path="C:/ti/ccsv8/tools/compiler/ti-cgt-c2000_18.1.1.LTS/include" --define=EMPL_TARGET_F28379D --define=MPU6050 --define=DEBUG --define=_FLASH --define=CPU1 --diag_suppress=10063 --diag_warning=225 --diag_wrap=off --display_error_number --preproc_with_compile --preproc_dependency="eMD-6.0/driver/eMPL/inv_mpu_dmp_motion_driver.d_raw" --obj_directory="eMD-6.0/driver/eMPL" $(GEN_OPTS__FLAG) "$<"
	@echo 'Finished building: "$<"'
	@echo ' '


