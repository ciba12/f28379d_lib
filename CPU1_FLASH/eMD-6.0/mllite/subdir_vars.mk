################################################################################
# Automatically-generated file. Do not edit!
################################################################################

SHELL = cmd.exe

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../eMD-6.0/mllite/data_builder.c \
../eMD-6.0/mllite/hal_outputs.c \
../eMD-6.0/mllite/message_layer.c \
../eMD-6.0/mllite/ml_math_func.c \
../eMD-6.0/mllite/mlmath.c \
../eMD-6.0/mllite/mpl.c \
../eMD-6.0/mllite/results_holder.c \
../eMD-6.0/mllite/start_manager.c \
../eMD-6.0/mllite/storage_manager.c 

C_DEPS += \
./eMD-6.0/mllite/data_builder.d \
./eMD-6.0/mllite/hal_outputs.d \
./eMD-6.0/mllite/message_layer.d \
./eMD-6.0/mllite/ml_math_func.d \
./eMD-6.0/mllite/mlmath.d \
./eMD-6.0/mllite/mpl.d \
./eMD-6.0/mllite/results_holder.d \
./eMD-6.0/mllite/start_manager.d \
./eMD-6.0/mllite/storage_manager.d 

OBJS += \
./eMD-6.0/mllite/data_builder.obj \
./eMD-6.0/mllite/hal_outputs.obj \
./eMD-6.0/mllite/message_layer.obj \
./eMD-6.0/mllite/ml_math_func.obj \
./eMD-6.0/mllite/mlmath.obj \
./eMD-6.0/mllite/mpl.obj \
./eMD-6.0/mllite/results_holder.obj \
./eMD-6.0/mllite/start_manager.obj \
./eMD-6.0/mllite/storage_manager.obj 

OBJS__QUOTED += \
"eMD-6.0\mllite\data_builder.obj" \
"eMD-6.0\mllite\hal_outputs.obj" \
"eMD-6.0\mllite\message_layer.obj" \
"eMD-6.0\mllite\ml_math_func.obj" \
"eMD-6.0\mllite\mlmath.obj" \
"eMD-6.0\mllite\mpl.obj" \
"eMD-6.0\mllite\results_holder.obj" \
"eMD-6.0\mllite\start_manager.obj" \
"eMD-6.0\mllite\storage_manager.obj" 

C_DEPS__QUOTED += \
"eMD-6.0\mllite\data_builder.d" \
"eMD-6.0\mllite\hal_outputs.d" \
"eMD-6.0\mllite\message_layer.d" \
"eMD-6.0\mllite\ml_math_func.d" \
"eMD-6.0\mllite\mlmath.d" \
"eMD-6.0\mllite\mpl.d" \
"eMD-6.0\mllite\results_holder.d" \
"eMD-6.0\mllite\start_manager.d" \
"eMD-6.0\mllite\storage_manager.d" 

C_SRCS__QUOTED += \
"../eMD-6.0/mllite/data_builder.c" \
"../eMD-6.0/mllite/hal_outputs.c" \
"../eMD-6.0/mllite/message_layer.c" \
"../eMD-6.0/mllite/ml_math_func.c" \
"../eMD-6.0/mllite/mlmath.c" \
"../eMD-6.0/mllite/mpl.c" \
"../eMD-6.0/mllite/results_holder.c" \
"../eMD-6.0/mllite/start_manager.c" \
"../eMD-6.0/mllite/storage_manager.c" 


