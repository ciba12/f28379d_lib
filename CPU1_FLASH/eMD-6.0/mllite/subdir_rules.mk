################################################################################
# Automatically-generated file. Do not edit!
################################################################################

SHELL = cmd.exe

# Each subdirectory must supply rules for building sources it contributes
eMD-6.0/mllite/data_builder.obj: ../eMD-6.0/mllite/data_builder.c $(GEN_OPTS) | $(GEN_HDRS)
	@echo 'Building file: "$<"'
	@echo 'Invoking: C2000 Compiler'
	"C:/ti/ccsv8/tools/compiler/ti-cgt-c2000_18.1.1.LTS/bin/cl2000" -v28 -ml -mt --cla_support=cla1 --float_support=fpu32 --tmu_support=tmu0 --vcu_support=vcu2 -Ooff --include_path="E:/c2000_workspace/MPU6000_SPI_lib" --include_path="E:/c2000_workspace/MPU6000_SPI_lib/eMD-6.0/driver/include" --include_path="E:/c2000_workspace/MPU6000_SPI_lib/eMD-6.0/mllite" --include_path="E:/c2000_workspace/MPU6000_SPI_lib/eMD-6.0/driver/F28379D_api" --include_path="E:/c2000_workspace/MPU6000_SPI_lib/device" --include_path="C:/ti/c2000/C2000Ware_1_00_05_00/driverlib/f2837xd/driverlib" --include_path="C:/ti/ccsv8/tools/compiler/ti-cgt-c2000_18.1.1.LTS/include" --define=EMPL_TARGET_F28379D --define=MPU6050 --define=DEBUG --define=_FLASH --define=CPU1 --diag_suppress=10063 --diag_warning=225 --diag_wrap=off --display_error_number --preproc_with_compile --preproc_dependency="eMD-6.0/mllite/data_builder.d_raw" --obj_directory="eMD-6.0/mllite" $(GEN_OPTS__FLAG) "$<"
	@echo 'Finished building: "$<"'
	@echo ' '

eMD-6.0/mllite/hal_outputs.obj: ../eMD-6.0/mllite/hal_outputs.c $(GEN_OPTS) | $(GEN_HDRS)
	@echo 'Building file: "$<"'
	@echo 'Invoking: C2000 Compiler'
	"C:/ti/ccsv8/tools/compiler/ti-cgt-c2000_18.1.1.LTS/bin/cl2000" -v28 -ml -mt --cla_support=cla1 --float_support=fpu32 --tmu_support=tmu0 --vcu_support=vcu2 -Ooff --include_path="E:/c2000_workspace/MPU6000_SPI_lib" --include_path="E:/c2000_workspace/MPU6000_SPI_lib/eMD-6.0/driver/include" --include_path="E:/c2000_workspace/MPU6000_SPI_lib/eMD-6.0/mllite" --include_path="E:/c2000_workspace/MPU6000_SPI_lib/eMD-6.0/driver/F28379D_api" --include_path="E:/c2000_workspace/MPU6000_SPI_lib/device" --include_path="C:/ti/c2000/C2000Ware_1_00_05_00/driverlib/f2837xd/driverlib" --include_path="C:/ti/ccsv8/tools/compiler/ti-cgt-c2000_18.1.1.LTS/include" --define=EMPL_TARGET_F28379D --define=MPU6050 --define=DEBUG --define=_FLASH --define=CPU1 --diag_suppress=10063 --diag_warning=225 --diag_wrap=off --display_error_number --preproc_with_compile --preproc_dependency="eMD-6.0/mllite/hal_outputs.d_raw" --obj_directory="eMD-6.0/mllite" $(GEN_OPTS__FLAG) "$<"
	@echo 'Finished building: "$<"'
	@echo ' '

eMD-6.0/mllite/message_layer.obj: ../eMD-6.0/mllite/message_layer.c $(GEN_OPTS) | $(GEN_HDRS)
	@echo 'Building file: "$<"'
	@echo 'Invoking: C2000 Compiler'
	"C:/ti/ccsv8/tools/compiler/ti-cgt-c2000_18.1.1.LTS/bin/cl2000" -v28 -ml -mt --cla_support=cla1 --float_support=fpu32 --tmu_support=tmu0 --vcu_support=vcu2 -Ooff --include_path="E:/c2000_workspace/MPU6000_SPI_lib" --include_path="E:/c2000_workspace/MPU6000_SPI_lib/eMD-6.0/driver/include" --include_path="E:/c2000_workspace/MPU6000_SPI_lib/eMD-6.0/mllite" --include_path="E:/c2000_workspace/MPU6000_SPI_lib/eMD-6.0/driver/F28379D_api" --include_path="E:/c2000_workspace/MPU6000_SPI_lib/device" --include_path="C:/ti/c2000/C2000Ware_1_00_05_00/driverlib/f2837xd/driverlib" --include_path="C:/ti/ccsv8/tools/compiler/ti-cgt-c2000_18.1.1.LTS/include" --define=EMPL_TARGET_F28379D --define=MPU6050 --define=DEBUG --define=_FLASH --define=CPU1 --diag_suppress=10063 --diag_warning=225 --diag_wrap=off --display_error_number --preproc_with_compile --preproc_dependency="eMD-6.0/mllite/message_layer.d_raw" --obj_directory="eMD-6.0/mllite" $(GEN_OPTS__FLAG) "$<"
	@echo 'Finished building: "$<"'
	@echo ' '

eMD-6.0/mllite/ml_math_func.obj: ../eMD-6.0/mllite/ml_math_func.c $(GEN_OPTS) | $(GEN_HDRS)
	@echo 'Building file: "$<"'
	@echo 'Invoking: C2000 Compiler'
	"C:/ti/ccsv8/tools/compiler/ti-cgt-c2000_18.1.1.LTS/bin/cl2000" -v28 -ml -mt --cla_support=cla1 --float_support=fpu32 --tmu_support=tmu0 --vcu_support=vcu2 -Ooff --include_path="E:/c2000_workspace/MPU6000_SPI_lib" --include_path="E:/c2000_workspace/MPU6000_SPI_lib/eMD-6.0/driver/include" --include_path="E:/c2000_workspace/MPU6000_SPI_lib/eMD-6.0/mllite" --include_path="E:/c2000_workspace/MPU6000_SPI_lib/eMD-6.0/driver/F28379D_api" --include_path="E:/c2000_workspace/MPU6000_SPI_lib/device" --include_path="C:/ti/c2000/C2000Ware_1_00_05_00/driverlib/f2837xd/driverlib" --include_path="C:/ti/ccsv8/tools/compiler/ti-cgt-c2000_18.1.1.LTS/include" --define=EMPL_TARGET_F28379D --define=MPU6050 --define=DEBUG --define=_FLASH --define=CPU1 --diag_suppress=10063 --diag_warning=225 --diag_wrap=off --display_error_number --preproc_with_compile --preproc_dependency="eMD-6.0/mllite/ml_math_func.d_raw" --obj_directory="eMD-6.0/mllite" $(GEN_OPTS__FLAG) "$<"
	@echo 'Finished building: "$<"'
	@echo ' '

eMD-6.0/mllite/mlmath.obj: ../eMD-6.0/mllite/mlmath.c $(GEN_OPTS) | $(GEN_HDRS)
	@echo 'Building file: "$<"'
	@echo 'Invoking: C2000 Compiler'
	"C:/ti/ccsv8/tools/compiler/ti-cgt-c2000_18.1.1.LTS/bin/cl2000" -v28 -ml -mt --cla_support=cla1 --float_support=fpu32 --tmu_support=tmu0 --vcu_support=vcu2 -Ooff --include_path="E:/c2000_workspace/MPU6000_SPI_lib" --include_path="E:/c2000_workspace/MPU6000_SPI_lib/eMD-6.0/driver/include" --include_path="E:/c2000_workspace/MPU6000_SPI_lib/eMD-6.0/mllite" --include_path="E:/c2000_workspace/MPU6000_SPI_lib/eMD-6.0/driver/F28379D_api" --include_path="E:/c2000_workspace/MPU6000_SPI_lib/device" --include_path="C:/ti/c2000/C2000Ware_1_00_05_00/driverlib/f2837xd/driverlib" --include_path="C:/ti/ccsv8/tools/compiler/ti-cgt-c2000_18.1.1.LTS/include" --define=EMPL_TARGET_F28379D --define=MPU6050 --define=DEBUG --define=_FLASH --define=CPU1 --diag_suppress=10063 --diag_warning=225 --diag_wrap=off --display_error_number --preproc_with_compile --preproc_dependency="eMD-6.0/mllite/mlmath.d_raw" --obj_directory="eMD-6.0/mllite" $(GEN_OPTS__FLAG) "$<"
	@echo 'Finished building: "$<"'
	@echo ' '

eMD-6.0/mllite/mpl.obj: ../eMD-6.0/mllite/mpl.c $(GEN_OPTS) | $(GEN_HDRS)
	@echo 'Building file: "$<"'
	@echo 'Invoking: C2000 Compiler'
	"C:/ti/ccsv8/tools/compiler/ti-cgt-c2000_18.1.1.LTS/bin/cl2000" -v28 -ml -mt --cla_support=cla1 --float_support=fpu32 --tmu_support=tmu0 --vcu_support=vcu2 -Ooff --include_path="E:/c2000_workspace/MPU6000_SPI_lib" --include_path="E:/c2000_workspace/MPU6000_SPI_lib/eMD-6.0/driver/include" --include_path="E:/c2000_workspace/MPU6000_SPI_lib/eMD-6.0/mllite" --include_path="E:/c2000_workspace/MPU6000_SPI_lib/eMD-6.0/driver/F28379D_api" --include_path="E:/c2000_workspace/MPU6000_SPI_lib/device" --include_path="C:/ti/c2000/C2000Ware_1_00_05_00/driverlib/f2837xd/driverlib" --include_path="C:/ti/ccsv8/tools/compiler/ti-cgt-c2000_18.1.1.LTS/include" --define=EMPL_TARGET_F28379D --define=MPU6050 --define=DEBUG --define=_FLASH --define=CPU1 --diag_suppress=10063 --diag_warning=225 --diag_wrap=off --display_error_number --preproc_with_compile --preproc_dependency="eMD-6.0/mllite/mpl.d_raw" --obj_directory="eMD-6.0/mllite" $(GEN_OPTS__FLAG) "$<"
	@echo 'Finished building: "$<"'
	@echo ' '

eMD-6.0/mllite/results_holder.obj: ../eMD-6.0/mllite/results_holder.c $(GEN_OPTS) | $(GEN_HDRS)
	@echo 'Building file: "$<"'
	@echo 'Invoking: C2000 Compiler'
	"C:/ti/ccsv8/tools/compiler/ti-cgt-c2000_18.1.1.LTS/bin/cl2000" -v28 -ml -mt --cla_support=cla1 --float_support=fpu32 --tmu_support=tmu0 --vcu_support=vcu2 -Ooff --include_path="E:/c2000_workspace/MPU6000_SPI_lib" --include_path="E:/c2000_workspace/MPU6000_SPI_lib/eMD-6.0/driver/include" --include_path="E:/c2000_workspace/MPU6000_SPI_lib/eMD-6.0/mllite" --include_path="E:/c2000_workspace/MPU6000_SPI_lib/eMD-6.0/driver/F28379D_api" --include_path="E:/c2000_workspace/MPU6000_SPI_lib/device" --include_path="C:/ti/c2000/C2000Ware_1_00_05_00/driverlib/f2837xd/driverlib" --include_path="C:/ti/ccsv8/tools/compiler/ti-cgt-c2000_18.1.1.LTS/include" --define=EMPL_TARGET_F28379D --define=MPU6050 --define=DEBUG --define=_FLASH --define=CPU1 --diag_suppress=10063 --diag_warning=225 --diag_wrap=off --display_error_number --preproc_with_compile --preproc_dependency="eMD-6.0/mllite/results_holder.d_raw" --obj_directory="eMD-6.0/mllite" $(GEN_OPTS__FLAG) "$<"
	@echo 'Finished building: "$<"'
	@echo ' '

eMD-6.0/mllite/start_manager.obj: ../eMD-6.0/mllite/start_manager.c $(GEN_OPTS) | $(GEN_HDRS)
	@echo 'Building file: "$<"'
	@echo 'Invoking: C2000 Compiler'
	"C:/ti/ccsv8/tools/compiler/ti-cgt-c2000_18.1.1.LTS/bin/cl2000" -v28 -ml -mt --cla_support=cla1 --float_support=fpu32 --tmu_support=tmu0 --vcu_support=vcu2 -Ooff --include_path="E:/c2000_workspace/MPU6000_SPI_lib" --include_path="E:/c2000_workspace/MPU6000_SPI_lib/eMD-6.0/driver/include" --include_path="E:/c2000_workspace/MPU6000_SPI_lib/eMD-6.0/mllite" --include_path="E:/c2000_workspace/MPU6000_SPI_lib/eMD-6.0/driver/F28379D_api" --include_path="E:/c2000_workspace/MPU6000_SPI_lib/device" --include_path="C:/ti/c2000/C2000Ware_1_00_05_00/driverlib/f2837xd/driverlib" --include_path="C:/ti/ccsv8/tools/compiler/ti-cgt-c2000_18.1.1.LTS/include" --define=EMPL_TARGET_F28379D --define=MPU6050 --define=DEBUG --define=_FLASH --define=CPU1 --diag_suppress=10063 --diag_warning=225 --diag_wrap=off --display_error_number --preproc_with_compile --preproc_dependency="eMD-6.0/mllite/start_manager.d_raw" --obj_directory="eMD-6.0/mllite" $(GEN_OPTS__FLAG) "$<"
	@echo 'Finished building: "$<"'
	@echo ' '

eMD-6.0/mllite/storage_manager.obj: ../eMD-6.0/mllite/storage_manager.c $(GEN_OPTS) | $(GEN_HDRS)
	@echo 'Building file: "$<"'
	@echo 'Invoking: C2000 Compiler'
	"C:/ti/ccsv8/tools/compiler/ti-cgt-c2000_18.1.1.LTS/bin/cl2000" -v28 -ml -mt --cla_support=cla1 --float_support=fpu32 --tmu_support=tmu0 --vcu_support=vcu2 -Ooff --include_path="E:/c2000_workspace/MPU6000_SPI_lib" --include_path="E:/c2000_workspace/MPU6000_SPI_lib/eMD-6.0/driver/include" --include_path="E:/c2000_workspace/MPU6000_SPI_lib/eMD-6.0/mllite" --include_path="E:/c2000_workspace/MPU6000_SPI_lib/eMD-6.0/driver/F28379D_api" --include_path="E:/c2000_workspace/MPU6000_SPI_lib/device" --include_path="C:/ti/c2000/C2000Ware_1_00_05_00/driverlib/f2837xd/driverlib" --include_path="C:/ti/ccsv8/tools/compiler/ti-cgt-c2000_18.1.1.LTS/include" --define=EMPL_TARGET_F28379D --define=MPU6050 --define=DEBUG --define=_FLASH --define=CPU1 --diag_suppress=10063 --diag_warning=225 --diag_wrap=off --display_error_number --preproc_with_compile --preproc_dependency="eMD-6.0/mllite/storage_manager.d_raw" --obj_directory="eMD-6.0/mllite" $(GEN_OPTS__FLAG) "$<"
	@echo 'Finished building: "$<"'
	@echo ' '


